console.info("--Array--");
// Declare a fruits array
var fruits = ["Apple", "Banana"];
// Print of the size of the fruits
console.info(fruits.length);
var first =fruits[0];
console.info(first);
var last =  fruits[fruits.length-1];
console.info(last);
// Iterate the fruits array
fruits.forEach( function (item, index2, array){
    console.info("Index : " + index2);
    console.info("Value : " + item);
});

for(var x =0 ; x < fruits.length; x++){
    console.log(fruits[x]);
}

var newLength = fruits.push("Durian");
console.info("New length : " + newLength);

for(var x =0 ; x < fruits.length; x++){
    console.log(fruits[x]);
}
console.log("----------------");
var newLength = fruits.pop();
for(var x =0 ; x < fruits.length; x++){
    console.log(fruits[x]);
}
console.log("----------------");
var first = fruits.shift();
//console.info(first);
for(var x =0 ; x < fruits.length; x++){
    console.log(fruits[x]);
}
console.log("----------------");
var newLength = fruits.unshift("Mangoesteen");
for(var x =0 ; x < fruits.length; x++){
    console.log(fruits[x]);
}

console.info("Index of Banana " + fruits.indexOf("Banana"));
console.log("----------------");
// Clone a shallow array
var shallowCopy  = fruits.slice();
shallowCopy.unshift("Watermelon");
console.log("-------fruits-----");

for(var x =0 ; x < fruits.length; x++){
    console.log(fruits[x]);
}

console.log("-------shallowCopy-----");
for(var x =0 ; x < shallowCopy.length; x++){
    console.log(shallowCopy[x]);
}

console.log("-------shallowCopy2-----");
var shallowCopy2 = fruits;
for(var x =0 ; x < shallowCopy2.length; x++){
    console.log(shallowCopy2[x]);
}

console.log("-------shallowCopy2 #2-----");
fruits.unshift("Orange");
for(var x =0 ; x < shallowCopy2.length; x++){
    console.log(shallowCopy2[x]);
}

console.log("-------fruits #2-----");
for(var x =0 ; x < fruits.length; x++){
    console.log(fruits[x]);
}

console.log("-------fruits #3-----");
shallowCopy2.unshift("Raspberry");
for(var x =0 ; x < fruits.length; x++){
    console.log(fruits[x]);
}

console.log("-------fruits #4-----");
var shallowCopy3  = fruits.slice();
shallowCopy3.unshift("Papaya");
for(var x =0 ; x < fruits.length; x++){
    console.log(fruits[x]);
}

console.log("------------------");

for(var x =0 ; x < shallowCopy3.length; x++){
    console.log(shallowCopy3[x]);
}

console.log("----------------");
console.log("----------------" + fruits.length);
var newLength = fruits.unshift("Dragon Fruit");
console.log("----------------" + newLength);

console.log("-------dictionaries------");
var dict = { FirstName: "Kenneth",
            "one": 1,
            "1": "some values"
            };

//Accessing values
console.info(dict["one"]);
console.info(dict.FirstName);
console.info(dict["1"]);

// Write values
dict.FirstName = "David";
console.info(dict.FirstName);

console.log("-------dictionaries #2------");


for(var key in dict){
    var value = dict[key];
    console.info(value);
}
console.log("-------dictionaries #3------");

console.info(dict['one']);

var alphabets = ['B', 'C', 'D', 'A'];
console.info("Before...");
for(var x =0 ; x < alphabets.length; x++){
    console.log(alphabets[x]);
}
alphabets.sort();
console.info("after sort...");
for(var x =0 ; x < alphabets.length; x++){
    console.log(alphabets[x]);
}

var tobeSortArray = [];
console.info("Before tobeSort...");
for(var key in dict){
    var value = dict[key];
    console.info(value);
    tobeSortArray.push(value); // push to be sort array
}
console.info("After tobeSort...");
tobeSortArray.sort();
for(var x =0 ; x < tobeSortArray.length; x++){
    console.log(tobeSortArray[x]);
}
console.info("----- Reverse tobeSort ----");
tobeSortArray.reverse();
for(var x =0 ; x < tobeSortArray.length; x++){
    console.log(tobeSortArray[x]);
}

console.info("----- Smallest to Big ----");

var pts = [40, 30, 20, 10, 1, 3, 6];

pts.sort(function(a, b){
    return (b-a);
});

for(var x =0 ; x < pts.length; x++){
    console.log(pts[x]);
}



